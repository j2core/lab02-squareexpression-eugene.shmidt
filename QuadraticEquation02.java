package com.j2core.shmidt.week02.quadraticEquation02;

import java.util.NoSuchElementException;
import java.util.Scanner;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static java.lang.StrictMath.abs;

/**
 * Calculate quadratic equation ax^2 + bx + c = 0
 *
 * Example1 two roots: x^2+3x-4 = 0 printing: The equation has two roots x1=  1.0 and x2 = -4.0
 * Example2 one root: x^2+6x+9 = 0 printing: The equation has one root x1= -3.0
 * Example3 no roots: x^2+x+1 = 0 printing: The equation has no real roots
 */
public class QuadraticEquation02 {

        public static void main(String args[]){

            double a;
            double b;
            double c;
            double x1;
            double x2;
            double discriminant;
            final double EPSILON; //the value could be equal to 0.00000001;

            InputParameter ip = new InputParameter();

            System.out.println("Please enter EPSILON parameter(MIN positive value for calculation from 1E-3 till 1E-16)");
            EPSILON = ip.obtainInputParameter();
            System.out.println("Please type parameters one-by-one or Write 'exit' for exit-out from the program");
            System.out.println("Please enter the first 'A' parameter and press Enter: ");
            a = ip.obtainInputParameter();
            System.out.println("Please enter the second 'B' parameter and press Enter: ");
            b = ip.obtainInputParameter();
            System.out.println("Please enter the third 'C' parameter and press Enter: ");
            c = ip.obtainInputParameter();

                if (abs(a) == 0) {
                    if (abs(b) == 0){
                        System.out.printf("'b' parameter cannot be %s ", b);
                    } else {
                        x1 = -c / b;
                        System.out.printf("a = %s the equation is not quadratic, the function is Liniar x = %.2f", a, x1);
                    }}  else {
                    discriminant = pow(b, 2) - (4 * a * c);
                    if (discriminant > EPSILON) {
                        double sqr_d = sqrt(discriminant);
                        x1 = (-b + sqr_d) / (2 * a);
                        x2 = (-b - sqr_d) / (2 * a);
                        System.out.printf("The equation has two roots: x1 = %s and x2 = %s", x1, x2);
                    } else if   (abs(discriminant) < EPSILON && abs(discriminant) > -EPSILON ) {
                        x1 = -b / 2 * a;
                        System.out.printf("The equation has only one root: x1 & x2 = %s ", x1);
                    }  else {
                        System.out.print("The quadratic equation has no real roots!");
                    }
                }
        }

}

 class InputParameter {

     public double obtainInputParameter() {
         String input = "";
         double output = 0.0;
         Scanner scanner = new Scanner(System.in);

         while (!scanner.hasNextDouble()) {
             input = scanner.next();
             try {
                 if (input.equals("exit")) {
                     System.out.println("You just exit out from the program!");
                     System.exit(0);
                 } else
                     return Double.parseDouble(input);

             } catch ( NumberFormatException  e) {
                 System.err.println("The input is not a number, please enter the number or type 'exit' for exit-out");
             }
         }
         try {
             output = scanner.nextDouble();
         } catch(NoSuchElementException e){
             e.printStackTrace();
         }
         return output;
     }
 }